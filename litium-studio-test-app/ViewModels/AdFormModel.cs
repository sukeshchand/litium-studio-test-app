﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class AdFormModel
    {
        public string AdHeader { get; set; }
        public string AddSubHeader { get; set; }
        public bool IsShowSubHeader { get; set; }
    }
}