﻿using System.Collections.Generic;
using System.Web.Mvc;
using litium_studio_test_app.PageTypes;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS.Pages;
namespace litium_studio_test_app.Controllers
{
   public class SimpleAdsPageTypeController : Controller
        {
            public ActionResult AdPage(SimpleAdsPageType def, Page currentPage)
            {
                var model = new AdFormModel()
                {
                    AdHeader = def.PageHeader,
                    AddSubHeader = def.PageSubHeader,
                    IsShowSubHeader = def.IsSubHeaderRequired
                };
                
                return View(model);
            }
        }
    }


