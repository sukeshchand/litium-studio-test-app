﻿namespace litium_studio_test_app.PageTypes
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class SingleInstanceAttribute : System.Attribute
    {
    }
}
